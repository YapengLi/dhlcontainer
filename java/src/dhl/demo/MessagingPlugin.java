package dhl.demo;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.kafka.clients.producer.ProducerRecord;

import com.apama.epl.plugin.annotation.*;
import com.apama.jmon.annotation.*;
import com.apama.util.Logger;

@Application(name = "MessagingPlugin", 
	author = "", 
	version = "1.0", 
	company = "", 
	description = "", 
	classpath = "lib/kafka-clients-2.0.0.jar;lib/snappy-java-1.1.7.1.jar")                


@EPLPlugin(description = "Manages consumer and producer", name= "dhl.demo.MessagingPlugin")
public class MessagingPlugin {

	private static Logger logger = Logger.getLogger();

	private static Map<String, TopicConsumerRunnable> topicConsumers = new ConcurrentHashMap<String, TopicConsumerRunnable>();

	private static Map<String, Boolean> consumersStarted = new ConcurrentHashMap<String, Boolean>();

	private static Map<String, TopicProducer> topicProducers = new ConcurrentHashMap<String, TopicProducer>();

	private static String bootstrapServers;

	private static boolean initialised;
	private static boolean started;
	
	public static boolean isInitialised() {
		return initialised;
	}

	public static boolean isStarted() {
		return started;
	}
	
	public static void initialise(String bootstrapServers) {
		if ("".equals(bootstrapServers)) {
			bootstrapServers = "my-cluster-kafka-bootstrap:9092,my-cluster-kafka:9092";
		}
		MessagingPlugin.bootstrapServers = bootstrapServers;
		
		initialised = true;
		started = false;
						
		logger.info("Messaging plugin initialised, bootstrap servers = "+bootstrapServers);
	}
	
	public static boolean addProducer(String topic, String codeTopicMapping) {
		if (initialised) {
			if (!topicProducers.containsKey(codeTopicMapping)) {
				try {
					TopicProducer<String, String> producer = new TopicProducer<String, String>(bootstrapServers, topic);
					topicProducers.put(codeTopicMapping, producer);
					
					int i = 0;
					while (i < 100) {
						logger.info("ping "+i);
						i++;
					}
					
					logger.info("Created and added messaging producer on queue = "+topic+" with mapping = "+codeTopicMapping);
					return true;
				}
				catch (Exception ex) {
					logger.error("Issue creating producer for "+topic, ex);
				}
			} else {
				logger.warn("Not creating messaging producer as producer already exists on topic = "+topic+" using code topic mapping "+codeTopicMapping);
			}
		} else {
			logger.error("Not creating messaging producer as initialisation has not been received");
		}
		return false;
	}

	public static boolean addConsumer(String topic, String consumerGroup) {
		if (initialised) {
			if (!topicConsumers.containsKey(topic)) {
				TopicConsumerRunnable tcr = new TopicConsumerRunnable(bootstrapServers, topic, consumerGroup, true);

				topicConsumers.put(topic, tcr);
				consumersStarted.put(topic, false);
				logger.info("Added messaging consumer on topic = "+topic);
				return true;
			} else {
				logger.warn("Not creating messaging consumer as consumer already exists on queue = "+topic);
			}
		} else {
			logger.error("Not creating messaging consumer as initialisation has not been received");
		}
		return false;
	}

	public static boolean publishMessage(String topic, String value, boolean block) {
		if (initialised) {
			if (topicProducers.containsKey(topic)) {
				TopicProducer<String, String> topicProducer = topicProducers.get(topic);
				ProducerRecord<String, String> record = new ProducerRecord<String, String>(topicProducer.getTopic(), value);
				return topicProducer.publish(record, block);
			}
			else {
				logger.warn("Can not send message as no producer found for code mapped topic = "+topic);
			}
		}
		return false;
	}
	
	public static void start() {
		if ( !started ) {
			
			if (consumersStarted.size() > 0) {
				logger.info("Starting consumers");
			}
			for(String topicName : consumersStarted.keySet()) {
				if (!consumersStarted.get(topicName) && topicConsumers.containsKey(topicName)) {
					logger.info("Attempting to start consumer for topic = "+topicName);
					new Thread(topicConsumers.get(topicName)).start();
					consumersStarted.put(topicName, true);
				}
			}
			
			started = true;
		}
	}

	private static void stopConsumers() throws Exception {
		logger.info("Stopping Consumers");
		for (TopicConsumerRunnable consumer : topicConsumers.values()) {
			consumer.stopConsumer();
		}
	}
	
	private static void stopPublishers() {
		logger.info("Stopping Publishers");
		for (TopicProducer producer : topicProducers.values()) {
			producer.stop();
		}
	}
	
	@com.apama.epl.plugin.annotation.Callback(type=com.apama.epl.plugin.annotation.Callback.CBType.SHUTDOWN)
	public static void shutdown() {
		logger.info("Messaging plugin is shutting down");
		try {
			stopPublishers();
			stopConsumers();
		} catch(Exception ex) {
			logger.error("Caught exception when shutting down MessagingAdapter", ex);
		}
	}
}
